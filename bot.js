const { Client, Intents, MessageEmbed } = require('discord.js');
require('dotenv').config();

const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES] });

const statusMessages = [
    'Selling Nitro, Boosts, Profile Decorations, OTT Subscriptions & Other Services @Low Price',
    'Join our Discord server for exclusive offers!',
    'Providing top-notch services to Discord users!',
    'Helping you enhance your Discord experience!',
];

const activityTypes = ['PLAYING', 'CUSTOM'];

const serverId = '1087210662280843264';

client.on('ready', () => {
    console.clear();
    console.log(`Logged in bot as ${client.user.tag}!`);

    const guild = client.guilds.cache.get(serverId);
    const memberCount = guild ? guild.memberCount : 0;
    const randomStatus = statusMessages[Math.floor(Math.random() * statusMessages.length)].replace('{memberCount}', memberCount);
    const randomActivityType = activityTypes[Math.floor(Math.random() * activityTypes.length)];

    const bigImageUrl = "https://media.discordapp.net/attachments/1163022210932428860/1188790462337798154/creavite_proficon_nick_store.gif?ex=659bce64&is=65895964&hm=c7da5ffa2ac1dbb6fc020aae4206b7dff1172bc27a2e9db52f964ddf35fd437a&=&width=320&height=320";
    const smallImageUrl = "https://cdn.discordapp.com/emojis/785233158801981521.gif?size=128&quality=lossless";

    const embed = new MessageEmbed()
        .setTitle(randomStatus)
        .setThumbnail(bigImageUrl)
        .addField('Member Count', `${memberCount}/100 Members`)
        .setImage(smallImageUrl)
        .setColor('#00ff00')
        .setTimestamp()
        .setFooter('Join Server For More Details');

    // Set the bot's activity
    client.user.setActivity(randomStatus, { type: 'PLAYING' });
});

client.login(process.env.nickstoreToken);
